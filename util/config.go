package util

import (
	"github.com/spf13/viper"
)

type Config struct {
	LogLevel string `mapstructure:"LOG_LEVEL"`

	MQTTProtocol string `mapstructure:"MQTT_PROTOCOL"`
	MQTTHost     string `mapstructure:"MQTT_HOST"`
	MQTTPort     int16  `mapstructure:"MQTT_PORT"`
	MQTTClientId string `mapstructure:"MQTT_CLIENT_ID"`
	MQTTTopic    string `mapstructure:"MQTT_TOPIC"`
	MQTTQuiesce  uint   `mapstructure:"MQTT_QUIESCE"`

	InfluxDBProtocol string `mapstructure:"INFLUXDB_PROTOCOL"`
	InfluxDBHost     string `mapstructure:"INFLUXDB_HOST"`
	InfluxDBPort     int16  `mapstructure:"INFLUXDB_PORT"`
	InfluxDBToken    string `mapstructure:"INFLUXDB_TOKEN"`
	InfluxDBBucket   string `mapstructure:"INFLUXDB_BUCKET"`
	InfluxDBOrg      string `mapstructure:"INFLUXDB_ORG"`

	PrometheusPath string `mapstructure:"PROMETHEUS_PATH"`
	PrometheusPort int16  `mapstructure:"PROMETHEUS_PORT"`
}

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
