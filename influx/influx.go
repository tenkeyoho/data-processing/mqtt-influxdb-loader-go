package influx

import (
	"context"
	"fmt"
	"net/url"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	log "github.com/sirupsen/logrus"

	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/mqtt"
)

var writeAPI api.WriteAPIBlocking

func ConnectInflux(autToken string, bucket string, org string, uri *url.URL) (client influxdb2.Client) {
	client = influxdb2.NewClient(uri.String(), autToken)
	writeAPI = client.WriteAPIBlocking(org, bucket)

	log.WithFields(log.Fields{"Bucket": bucket, "Org": org, "URI": uri.String()}).Infof("Connected to Influx database")

	return client
}

func CreatePoint(data *mqtt.SensorData) (err error) {
	if data == nil {
		err = fmt.Errorf("nil pointer detected")
		log.WithError(err).Error("Cannot persist empty data point")

		return err
	}

	if len(data.Name) == 0 {
		err = fmt.Errorf("empty string detected")
		log.WithError(err).Error("Cannot persist data point without name")

		return err
	}

	if data.Value == 0 {
		log.WithField("Value", data.Value).Warn("Zero value detected")
	}

	point := influxdb2.NewPointWithMeasurement(data.Name).
		AddField("value", data.Value).
		SetTime(time.Now())

	if len(data.Unit) > 0 {
		point = point.AddTag("unit", data.Unit)
	}

	if len(data.Description) > 0 {
		point = point.AddField("description", data.Description)
	}

	err = writeAPI.WritePoint(context.Background(), point)
	if err != nil {
		log.WithError(err).Error("Could not persist data point")
	}

	return err
}
