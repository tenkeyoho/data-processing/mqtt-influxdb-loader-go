module gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/influxdata/influxdb-client-go/v2 v2.5.1
	github.com/influxdata/line-protocol v0.0.0-20210311194329-9aa0e372d097 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0 // indirect
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf // indirect
	golang.org/x/sys v0.0.0-20210917161153-d61c044b1678 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
