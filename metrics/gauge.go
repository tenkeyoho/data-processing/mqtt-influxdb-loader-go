package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func NewGauge(subsystem string, name string, help string) *Gauge {
	return &Gauge{GaugeVec: promauto.NewGaugeVec(gaugeOptions(subsystem, name, help), []string{"topic"})}
}

type Gauge struct {
	GaugeVec *prometheus.GaugeVec
}

func (c *Gauge) Inc(topic string) {
	c.GaugeVec.With(c.labels(topic)).Inc()
}

func (c *Gauge) Dec(topic string) {
	c.GaugeVec.With(c.labels(topic)).Dec()
}

func (c *Gauge) Set(topic string, value float64) {
	c.GaugeVec.With(c.labels(topic)).Set(value)
}

func (c *Gauge) labels(topic string) prometheus.Labels {
	return prometheus.Labels{"topic": topic}
}

func gaugeOptions(subsystem string, name string, help string) prometheus.GaugeOpts {
	return prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      name,
		Help:      help,
	}
}
