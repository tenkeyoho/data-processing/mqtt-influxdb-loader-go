package metrics

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func NewHistogram(subsystem string, name string, help string) *Histogram {
	return &Histogram{HistogramVec: promauto.NewHistogramVec(histogramOptions(subsystem, name, help), []string{"topic"})}
}

type Histogram struct {
	HistogramVec *prometheus.HistogramVec
}

func (c *Histogram) Observe(topic string, value float64) {
	c.HistogramVec.With(prometheus.Labels{"topic": topic}).Observe(value)
}

func (c *Histogram) Track(topic string, start time.Time) (ms float64) {
	end := time.Now()
	duration := end.Sub(start)
	ms = float64(duration / time.Millisecond)

	c.Observe(topic, ms)

	return ms
}

func histogramOptions(subsystem string, name string, help string) prometheus.HistogramOpts {
	return prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      name,
		Help:      help,
		Buckets:   prometheus.LinearBuckets(100, 150, 8),
	}
}
