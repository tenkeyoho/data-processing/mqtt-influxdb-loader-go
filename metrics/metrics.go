package metrics

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	namespace = "tenkeyoho"
	service   = "influxloader"
)

var Prom Metrics

type FailureMetrics struct {
	Persistence   *Counter
	Unmarshalling *Counter
}

type ConsumerMetrics struct {
	Total          *Counter
	Latency        *Histogram
	CurrentLatency *Gauge
	Failures       *FailureMetrics
}

type Metrics struct {
	ServiceName string
	Consumer    *ConsumerMetrics
}

func init() {
	InitMetrics()
}

func InitMetrics() {
	Prom = Metrics{
		ServiceName: service,
		Consumer: &ConsumerMetrics{
			Total:          NewCounter(service, "msg_consumption_total", "The total number of MQTT messages consumed"),
			Latency:        NewHistogram(service, "msg_consumption_latency", "The duration needed to consume a MQTT message"),
			CurrentLatency: NewGauge(service, "msg_consumption_current_latency", "The current duration needed to consume a MQTT message"),
			Failures: &FailureMetrics{
				Persistence:   NewCounter(service, "msg_persistence_failed", "The total number of MQTT messages failed to be persisted"),
				Unmarshalling: NewCounter(service, "msg_unmarshalling_failed", "The total number of MQTT messages failed to be unmarshalled"),
			},
		},
	}
}

func Export(promPath string, promPort int16) {
	promHost := fmt.Sprintf(":%d", promPort)

	go func() {
		// Provide metrics endpoint
		http.Handle(promPath, promhttp.Handler())
		_ = http.ListenAndServe(promHost, nil)
	}()

	log.WithFields(log.Fields{"Path": promPath, "Port": promPort, "ServiceName": Prom.ServiceName}).Info("Enabled Prometheus metrics endpoint")
}
