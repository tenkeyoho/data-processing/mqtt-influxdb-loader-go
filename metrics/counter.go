package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func NewCounter(subsystem string, name string, help string) *Counter {
	return &Counter{CounterVec: promauto.NewCounterVec(counterOptions(subsystem, name, help), []string{"topic"})}
}

type Counter struct {
	CounterVec *prometheus.CounterVec
}

func (c *Counter) Inc(topic string) {
	c.CounterVec.With(prometheus.Labels{"topic": topic}).Inc()
}

func counterOptions(subsystem string, name string, help string) prometheus.CounterOpts {
	return prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      name,
		Help:      help,
	}
}
