package main

import (
	"fmt"
	"net"
	"net/url"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/influx"
	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/metrics"
	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/mqtt"
	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/util"
)

var config util.Config

func init() {
	var (
		level log.Level

		err error
	)

	// load application config

	config, err = util.LoadConfig(".")
	if err != nil {
		log.Fatal("Cannot load config:", err)
	}

	// setup logging

	level, err = log.ParseLevel(config.LogLevel)
	if err != nil {
		return
	}

	log.SetLevel(level)

	log.SetFormatter(&log.TextFormatter{
		DisableColors:    false,
		FullTimestamp:    true,
		DisableTimestamp: false,
		ForceQuote:       true,
		PadLevelText:     true,
	})

	// init metrics

	metrics.Export(config.PrometheusPath, config.PrometheusPort)

	// try to connect MQTT broker

	err = checkPort(config.MQTTHost, config.MQTTPort)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"Host": config.MQTTHost, "Port": config.MQTTPort}).Fatal("MQTT broker not reachable")
	}

	// try to connect Influx database

	err = checkPort(config.InfluxDBHost, config.InfluxDBPort)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"Host": config.InfluxDBHost, "Port": config.InfluxDBPort}).Warn("Influx database not reachable")
	}
}

func main() {
	var (
		uri *url.URL

		err error
	)

	// MQTT Client

	uri, err = url.Parse(fmt.Sprintf("%s://%s:%d", config.MQTTProtocol, config.MQTTHost, config.MQTTPort))
	if err != nil {
		log.WithError(err).Fatalf("Cannot parse MQTT broker URI")
	}
	mqttClient := mqtt.ConnectMQTT(config.MQTTClientId, uri)
	defer mqttClient.Disconnect(config.MQTTQuiesce)

	// InfluxDB Client

	uri, err = url.Parse(fmt.Sprintf("%s://%s:%d", config.InfluxDBProtocol, config.InfluxDBHost, config.InfluxDBPort))
	if err != nil {
		log.WithError(err).Fatalf("Cannot parse Influx database URI")
	}
	influxClient := influx.ConnectInflux(config.InfluxDBToken, config.InfluxDBBucket, config.InfluxDBOrg, uri)
	defer influxClient.Close()

	// MQTT Consumer Loop

	mqtt.Subscribe(mqttClient, config.MQTTTopic, influx.CreatePoint)

	select {}
}

func checkPort(host string, port int16) error {
	timeout := time.Second
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, strconv.Itoa(int(port))), timeout)
	if err != nil {
		return err
	}

	err = conn.Close()
	return err
}
