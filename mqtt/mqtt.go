package mqtt

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	log "github.com/sirupsen/logrus"

	"gitlab.com/tenkeyoho/data-processing/mqtt-influxdb-loader-go/metrics"
)

var influx func(data *SensorData) error

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	var (
		topic   string
		payload []byte
		data    *SensorData
	)

	topic = msg.Topic()
	payload = msg.Payload()
	data = &SensorData{}

	logEntry := log.WithFields(log.Fields{"Topic": topic, "Payload": string(payload)})

	defer func(start time.Time) {
		ms := metrics.Prom.Consumer.Latency.Track(topic, start)
		metrics.Prom.Consumer.CurrentLatency.Set(topic, ms)
	}(time.Now())

	logEntry.Debug("Received data from MQTT broker")
	metrics.Prom.Consumer.Total.Inc(topic)

	err := json.Unmarshal(payload, &data)
	if err != nil {
		logEntry.WithError(err).Error("Cannot persist data from MQTT broker")
		metrics.Prom.Consumer.Failures.Unmarshalling.Inc(topic)
		return
	}

	logEntry = logEntry.WithFields(log.Fields{"Name": data.Name, "Value": data.Value, "Unit": data.Unit})
	logEntry.WithField("Description", data.Description).Debug("Unmarshalled sensor data")

	err = influx(data)
	if err != nil {
		logEntry.WithError(err).Error("Cannot persist data from MQTT broker")
		metrics.Prom.Consumer.Failures.Persistence.Inc(topic)
		return
	}

	logEntry.Info("Successfully persisted sensor data")
}

var onConnectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	reader := client.OptionsReader()
	fields := log.Fields{"ClientId": reader.ClientID(), "Servers": reader.Servers()}
	log.WithFields(fields).Info("Successfully connected to MQTT broker")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	reader := client.OptionsReader()
	log.WithError(err).WithField("ClientId", reader.ClientID()).Info("Connection to MQTT broker lost")
}

func createClientOptions(clientId string, uri *url.URL) (opts *mqtt.ClientOptions) {
	opts = mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetClientID(clientId)

	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = onConnectHandler
	opts.OnConnectionLost = connectLostHandler

	return opts
}

func ConnectMQTT(clientId string, uri *url.URL) (client mqtt.Client) {
	opts := createClientOptions(clientId, uri)
	client = mqtt.NewClient(opts)

	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		log.WithError(err).Fatal("Cannot connect MQTT broker")
	}

	return client
}

func Subscribe(client mqtt.Client, topic string, callback func(data *SensorData) error) {
	influx = callback

	token := client.Subscribe(topic, 1, nil)
	token.Wait()

	log.WithField("Topic", topic).Infof("Subscribed to MQTT topic")
}
