FROM golang:1.17-alpine

# Install packages
RUN apk update
RUN apk add make bash
RUN apk add ca-certificates git

# Move to working directory
RUN mkdir /go/src/mqtt-influxdb-loader
WORKDIR /go/src/mqtt-influxdb-loader

# Copy source code into container
COPY . .

# Download dependencies
RUN go get -d -v ./...

# Build
RUN go build -v -o main main.go

# Move to resulting binary folder
RUN mkdir /dist
WORKDIR /dist

# Copy binary from build to main folder
RUN cp -v /go/src/mqtt-influxdb-loader/main .
RUN cp -v /go/src/mqtt-influxdb-loader/*.env .

# Run
CMD ["/dist/main"]
