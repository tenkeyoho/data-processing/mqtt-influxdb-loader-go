ifneq (,$(wildcard ./.env))
    include .env
    export
    export PATH=${GOPATH}/bin:${GOROOT}/bin:$(shell printenv PATH)
endif

dockerize:
	docker build -t docker.io/meetuptalexdev/mqtt-influxdb-loader:latest .
	docker push docker.io/meetuptalexdev/mqtt-influxdb-loader:latest
